//Soal 1
console.log("Soal 1")

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"]
var collator = new Intl.Collator(undefined, {numeric: true, sensitivity: 'base'});
daftarBuah.sort(collator.compare)
daftarBuah.forEach(buah => {
    console.log(buah)
});
//Soal 2
console.log("\nSoal 2")
var kalimat = "saya sangat senang belajar javascript"
console.log(kalimat.split(" "))
//Soal 3
console.log("\nSoal 3")

var buah = [
    {
        'nama'          : 'strawberry',
        'warna'         : 'merah',
        'ada_bijinya'   : 'tidak',
        'harga'         : '9000'
    },
    {
        'nama'          : 'jeruk',
        'warna'         : 'oranye',
        'ada_bijinya'   : 'ada',
        'harga'         : '8000'
    },
    {
        'nama'          : 'Semangka',
        'warna'         : 'Hijau & Merah',
        'ada_bijinya'   : 'ada',
        'harga'         : '10000'
    },
    {
        'nama'          : 'Pisang',
        'warna'         : 'Kuning',
        'ada_bijinya'   : 'tidak',
        'harga'         : '5000'
    }
];
console.log(buah[0])