<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"></meta>
    <title>QUIZ 2 VUE</title>
</head>
<body>
    <div id="app">
        <h3>QUIZ 2 VUE</h3>

        <form @submit.prevent="process()">
            <input 
                v-model="form.name"
                type="textfield"
                class="form-control" 
            >   
            <button class="btn btn-primary" id="buttonP">add</button>
        </form> 

        <ul>
            <li v-for="(user,index) in users" style="margin-bottom: 20px;">
                @{{ user.name }} 
                <button type="button" v-on:click="edit(user,index)">edit</button> ||
                <button type="button" v-on:click="deletee(user,index)">delete</button>
            </li>
            <br>
        </ul>

    </div>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
    new Vue({
        el: "#app",
        data : {
            newUser : '',
            users : [],
            form : {name:''},
            indexUser: '',
            idUser: ''
        },
        mounted(){
            this.getAllUsers()
        },
        methods : {
            getAllUsers() {
                axios
                .get('/api/getAllUsers')
                .then( response => {
                    this.users = response.data.user
                })
            },
            addUser() {
                newUser = this.form.name.trim()
                if(newUser){
                    axios
                    .post('/api/addUser', {
                        'name' : newUser
                    })
                    .then(response => {
                        if(response.data.response_code == '00'){
                            this.getAllUsers()
                            this.form.name = ""
                        }
                    })
                }
            },
            editUser(){
                userNameNew = this.form.name.trim()
                if(userNameNew){
                    axios
                    .post('/api/updateUser/'+this.idUser, {
                        'name' : userNameNew
                    })
                    .then(response => {
                        if(response.data.response_code == '00'){
                            this.getAllUsers()
                            document.getElementById('buttonP').innerHTML = 'add'
                            this.form.name = ""
                        }
                    })
                }
            },
            process() {
                let buttonText = document.getElementById('buttonP').innerHTML
                if(buttonText == 'add'){
                    this.addUser()
                } else if(buttonText == 'update'){
                    this.editUser()
                }
            },
            edit(user,index){
                this.form.name = user.name
                this.indexUser = index
                this.idUser = user.id
                document.getElementById('buttonP').innerHTML = 'update'
            },
            deletee(user,index){
                if(confirm("Apakah Anda yakin Menghapus '"+user.name+"' ?")){

                    axios
                    .post('/api/deleteUser/'+user.id)
                    .then(response => {
                        if(response.data.response_code == '00'){
                            this.getAllUsers()
                        }
                    })
                }
            }
        }
    })
</script>
</body>
</html> 