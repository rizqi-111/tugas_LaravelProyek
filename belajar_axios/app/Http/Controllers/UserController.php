<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    //
    public function getAll(){
        $user = User::all();

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'Berhasil Mendapatkan Semua User',
            'user' => $user
        ]);
    }

    public function addUser(Request $request){
        
        $request->validate([
            'name' => ['required','max:255']
        ]);
        
        $user = User::create([
            'name' => $request->name
        ]);
        
        return response()->json([
            'response_code' => '00',
            'response_msg' => 'User Berhasil Ditambahkan',
            'user' => $user
        ]);

    }

    public function updateUser(Request $request, $id){

        $request->validate([
            'name' => ['required','max:255']
        ]);

        $user = User::find($id);

        $user->name = $request->name;
        $user->save();

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'User Berhasil Diubah',
            'user' => $user
        ]);

    }

    public function deleteUser($id){

        $user = User::find($id);
        $user->delete();

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'User Berhasil Dihapus',
            'user' => $user
        ]);
    }
}
